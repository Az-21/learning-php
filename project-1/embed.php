<?php

//Print
echo "Hello World<br>";

//Declaring Variables
$var1 = 100;
$var2 = 200;

//Concat
echo $var1 . " " . $var2 . "<br><br>";

//Math
echo 8+11 . "<br>";
echo 10-11 . "<br>";
echo 10*11 . "<br>";
echo 10/11 . "<br>";
echo (1+100+4+90) . "<br>";

//Array
$my_array1 = [1, 2, 3, 4];
$my_array2 = ["Hi", "Hello",  "<h1>Hi</h1>"];
$my_array3 = [1, "Hi"];

//Printing Array
print_r($my_array1);
echo "<br>";
print_r($my_array1[0]);
echo "<br>";


//Associtative Array
$aa1 = ["Name" => "Az21", "Age" => 21, "This was unassocitated"];
print_r($aa1);
echo "<br>";
print_r($aa1["Name"]);




?>
